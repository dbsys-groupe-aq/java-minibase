package iterator;

import java.lang.*;
import java.io.*;

/** 
   * @param filename
   * @param path
   * 
   * @exception FileNotFoundException the text file is not found
   */

public class RDFile {
    String[] attributeStrings;
    String[] databaseStrings;
    String[] condExprStrings;

    //constructor
    public RDFile (String filename, String path) throws FileNotFoundException {
        File file = new File(filename);
        String pth =  new String(path);

        if (file.exists()) {
            FileReader fr = new FileReader(pth+file);
            BufferedReader br = new BufferedReader(fr);
            try {
                
                String atr = br.readLine();
                attributeStrings = atr.trim().split("\\s+");

                String rd = br.readLine();
                databaseStrings = rd.trim().split("\\s+");

                String cexp = br.readLine();
                condExprStrings = cexp.trim().split("\\s+");
                
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null)
                        br.close();
                    if (fr != null)
                        fr.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else{
                System.out.println(filename + " does not exist");
                //System.exit(1);
            }
        }

    public String[] getAttributeStrings() {
        return attributeStrings;
    }

    public String[] getDBStrings() {
        return databaseStrings;
    }
    public String[] getConStrings() {
        return condExprStrings;
    }
}



    AttrType [] Stypes = {
        new AttrType(AttrType.attrInteger), 
        new AttrType(AttrType.attrInteger), 
        new AttrType(AttrType.attrInteger), 
        new AttrType(AttrType.attrInteger)
    };