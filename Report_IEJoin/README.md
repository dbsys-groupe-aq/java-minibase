# Instructions to run the tests for the different inequality join

## Tests

For this assignment, we created a new class called QueryIneqTest which contain an independent JoinsDriver, different queries methods (Query1a, Query1b, Query2a, Query2b and Query2c) and query condition expressions methods i.e. one for each query depending on the number of conditions.

To test the different implementations, use ONLY eclipse and execute the main of the QueryIneqTest class. We could not use make files because of some directory conflicts for the path given in QueryFile.

Be careful when running all the tests: a buffer manager error can be output soon. It is encouraged to run each test independantly  and comment the others in runTests.

## Query

To change the query text file give input the appropriate path and filename to the method QueryFile from the class QueryFile (this class can be found in Code_IEJoin directory). The number of conditions should also be given.

## IESelfJoin

Our class IESelfJoin implement the two different cases: Single and Two predicate. That's why the number of conditions should be precised in argument.

## Database 

A class has been written to read the attribute types of the Relational Database and chose the number of instances.

## Output files

For 2b and 2c output files, only 400 tuples in entry were used because of BufMgr Error.

## Experiment files

The experiments files used to get the plot are in Experiments directory.
